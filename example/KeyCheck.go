import (
	"crypto"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"encoding/base64"
	"strings"
)

const (
	rsaPublicKeyString = `MIGJAoGBALIEG+2iLpMPDqsqr3onEUHu+QtdRANEC6TBl7UtEAm1WondcyAiXsiIYmAZgptOcoLBZH6IfqmoXsv/DbVOdTbyQcgRntwqYEk4mh/boQHbCyOC7bVaoVGEAyaSNDW0WeBaUzAHfa3D8Fe6eDdyvKB2imhS2338ndYvT5dNNUV5AgMBAAE=`
)

var (
	rsaPublicKey *rsa.PublicKey
)

func authUid(uid string, key string) bool {
	if rsaPublicKey == nil {
		keyBytes, _ := base64.StdEncoding.DecodeString(rsaPublicKeyString)
		rsaPublicKey, _ = x509.ParsePKCS1PublicKey(keyBytes)
	}
	uidHash := sha256.Sum256([]byte(uid))
	sig, _ := base64.StdEncoding.DecodeString(strings.ReplaceAll(strings.ReplaceAll(key, "-", "+"), "_", "/"))
	err := rsa.VerifyPKCS1v15(rsaPublicKey, crypto.SHA256, uidHash[:], sig)
	return err == nil
}
